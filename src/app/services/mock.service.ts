import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Response} from '../models/response.model';

@Injectable({
    providedIn: 'root'
})
export class MockService {

    constructor(private http: HttpClient) { }

    getVoidResponseA(mockyDelay = 0): Observable<Response> {
        const params = new HttpParams()
            .append('mocky-delay', `${mockyDelay}s`)
        return this.http.get<Response>('https://run.mocky.io/v3/30051d25-2703-49da-9116-1d54c0732e1a', {params});
    }

    getVoidResponseB(mockyDelay = 0): Observable<Response> {
        const params = new HttpParams()
            .append('mocky-delay', `${mockyDelay}s`);
        return this.http.get<Response>('https://run.mocky.io/v3/d1ced416-ac8c-4825-876c-4e2084d4e45f', {params});
    }

    getVoidResponseC(mockyDelay = 0): Observable<Response> {
        const params = new HttpParams()
            .append('mocky-delay', `${mockyDelay}s`);
        return this.http.get<Response>('https://run.mocky.io/v3/80856105-7c94-4293-a03e-cb1b56e918bd', {params});
    }

    getVoidResponseD(mockyDelay = 0): Observable<Response> {
        const params = new HttpParams()
            .append('mocky-delay', `${mockyDelay}s`);
        return this.http.get<Response>('https://run.mocky.io/v3/e55ead47-7ac9-4ef3-9a06-568d3ac1798c', {params});
    }
}
