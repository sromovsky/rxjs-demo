import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ForkJoinComponent } from './pages/fork-join/fork-join.component';
import {HttpClientModule} from '@angular/common/http';
import { MergeMapComponent } from './pages/merge-map/merge-map.component';
import { ExamplesComponent } from './pages/examples/examples.component';

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        ForkJoinComponent,
        MergeMapComponent,
        ExamplesComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule
    ],
    providers: [],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
