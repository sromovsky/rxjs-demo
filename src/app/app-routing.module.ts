import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ForkJoinComponent} from './pages/fork-join/fork-join.component';
import {MergeMapComponent} from './pages/merge-map/merge-map.component';
import {ExamplesComponent} from './pages/examples/examples.component';

const routes: Routes = [
    {path: '', redirectTo: '/fork-join', pathMatch: 'full'},
    {path: 'fork-join', component: ForkJoinComponent},
    {path: 'merge-map', component: MergeMapComponent},
    {path: 'examples', component: ExamplesComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
