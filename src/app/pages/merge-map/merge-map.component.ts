import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {MockService} from '../../services/mock.service';
import {mergeMap} from 'rxjs/operators';

@Component({
    selector: 'app-merge-map',
    templateUrl: './merge-map.component.html'
})
export class MergeMapComponent implements OnInit, OnDestroy {

    #s: Subscription[];

    constructor(private mockService: MockService) {
        this.#s = [];
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.#s.forEach(s => s.unsubscribe());
    }

    example1() {
        this.#s.push(this.mockService.getVoidResponseA(1).subscribe(responseA => {
            console.log(responseA);

            this.#s.push(this.mockService.getVoidResponseB(1.5).subscribe(responseB => {
                console.log(responseB);
            }))
        }))
    }

    example2() {
        const observable = this.mockService.getVoidResponseA(1)
            .pipe(mergeMap(responseA => {
                console.log(responseA);

                return this.mockService.getVoidResponseB(1.5);
            }));

        this.#s.push(observable.subscribe(responseB => {
            console.log(responseB);
        }));
    }

    example3() {
        const observable = this.mockService.getVoidResponseA(1)
            .pipe(mergeMap(response1 => {
                console.log(response1);

                return this.mockService.getVoidResponseB(1)
            }));

        this.#s.push(this.mockService.getVoidResponseC(1)
            .pipe(mergeMap(response2 => {
                    console.log(response2);

                    return observable.pipe(mergeMap(response3 => {
                        console.log(response3);

                        return this.mockService.getVoidResponseD(1);
                    }));
                })
            ).subscribe(response4 => {
                console.log(response4);
            }));
    }
}
