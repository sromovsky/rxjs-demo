import {Component, OnDestroy, OnInit} from '@angular/core';
import {MockService} from '../../services/mock.service';
import {forkJoin, Subscription} from 'rxjs';

@Component({
    selector: 'app-fork-join',
    templateUrl: './fork-join.component.html'
})
export class ForkJoinComponent implements OnInit, OnDestroy {

    #s: Subscription[];

    constructor(private mockService: MockService) {
        this.#s = [];
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.#s.forEach(s => s.unsubscribe());
    }

    example1() {
        this.#s.push(this.mockService.getVoidResponseA(1).subscribe(responseA => {
            console.log(responseA);
        }));

        this.#s.push(this.mockService.getVoidResponseB(5).subscribe(responseB => {
            console.log(responseB);
        }));
    }

    example2() {
        this.#s.push(forkJoin([
            this.mockService.getVoidResponseA(1),
            this.mockService.getVoidResponseB(5)
        ]).subscribe(response => {
            console.log(response);
        }));
    }

    example3() {
        const a = forkJoin([
            this.mockService.getVoidResponseA(.5),
            this.mockService.getVoidResponseB(1)
        ]);

        this.#s.push(forkJoin([
            a,
            this.mockService.getVoidResponseC(2)
        ]).subscribe(response => {
            console.log(response);
        }));
    }

    example4() {
        const a = forkJoin([
            this.mockService.getVoidResponseA(.5),
            this.mockService.getVoidResponseB(1)
        ]);

        const b = forkJoin([
            this.mockService.getVoidResponseC(3),
            this.mockService.getVoidResponseD(4)
        ]);

        this.#s.push(a.subscribe(response1 => {
            console.log(response1);

            this.#s.push(b.subscribe(response2 => {
                console.log(response2);
            }));
        }));
    }
}
