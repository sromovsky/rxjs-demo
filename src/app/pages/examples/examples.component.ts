import {Component, OnDestroy, OnInit} from '@angular/core';
import {forkJoin, Subscription} from 'rxjs';
import {MockService} from '../../services/mock.service';
import {mergeMap} from 'rxjs/operators';

@Component({
    selector: 'app-examples',
    templateUrl: './examples.component.html'
})
export class ExamplesComponent implements OnInit, OnDestroy {

    #s: Subscription[];
    #example3Subscription: Subscription;

    constructor(private mockService: MockService) {
        this.#s = [];
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.#s.forEach(s => s.unsubscribe());
    }

    example1() {
        const serialBC = this.mockService.getVoidResponseB(1).pipe(mergeMap(responseB => {
            console.log(responseB);

            return this.mockService.getVoidResponseC(1);
        }))

        const parallelBCD = forkJoin([
            serialBC,
            this.mockService.getVoidResponseD(3)
        ]);

        const observable = this.mockService.getVoidResponseA(.5).pipe(mergeMap(responseA => {
            console.log(responseA);

            return parallelBCD.pipe(mergeMap(response => {
                console.log(response);

                return this.mockService.getVoidResponseA(.5);
            }));
        }))

        this.#s.push(observable.subscribe(response => {
            console.log(response);
        }))
    }

    example2() {
        const serialBC = this.mockService.getVoidResponseB(1).pipe(mergeMap(responseB => {
            console.log(responseB);

            return this.mockService.getVoidResponseC(1);
        }))

        const parallelBCD = forkJoin([
            serialBC,
            this.mockService.getVoidResponseD(1.5) // Change from example 1: (3s => 1.5s)
        ]);

        const observable = this.mockService.getVoidResponseA(.5).pipe(mergeMap(responseA => {
            console.log(responseA);

            return parallelBCD.pipe(mergeMap(response => {
                console.log(response);

                return this.mockService.getVoidResponseA(.5);
            }));
        }))

        this.#s.push(observable.subscribe(response => {
            console.log(response);
        }))
    }

    example3A() {
        this.#example3Subscription?.unsubscribe();

        this.#example3Subscription = forkJoin([
            this.mockService.getVoidResponseA(1),
            this.mockService.getVoidResponseB(3)
        ]).subscribe(response => {
            console.log(response);
        })
    }

    example3B() {
        this.#example3Subscription?.unsubscribe();

        this.#example3Subscription = this.mockService.getVoidResponseC(1).pipe(mergeMap(responseC => {
            console.log(responseC);

            return this.mockService.getVoidResponseD(3);
        })).subscribe(responseD => {
            console.log(responseD);
        })
    }
}
