import { Component, OnInit } from '@angular/core';
import {NavItem} from '../../models/nav-item.model';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

    navItems: NavItem[];

    constructor() { }

    ngOnInit(): void {
        this.navItems = [
            {link: '/fork-join', title: 'forkJoin'},
            {link: '/merge-map', title: 'mergeMap'},
            {link: '/examples', title: 'examples'}
        ]
    }
}
